//
//  BaseViewController.m
//  YelpOAuth
//
//  Created by Neil Bhargava on 7/26/14.
//  Copyright (c) 2014 Neil Bhargava. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

//This is for animating the textfields up and down when the keyboard is displayed on top of a textfield.
- (void)keyboardWillBeShown:(NSNotification*)aNotification moveView:(CGFloat)distance{
    //NSLog(@"Keyboard Showing");
    //NSDictionary* info = [aNotification userInfo];
    //CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    void (^animations)(void) = nil;
    animations = ^{
        // Resize the scroll view to make room for the keyboard
        CGRect aFrame = self.view.frame;
        aFrame.origin.y -= distance;//kbSize.height;
        [self.view setFrame: aFrame];
    };
    [UIView animateWithDuration:0.25 animations:animations];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification  moveView:(CGFloat)distance{
    // NSLog(@"Keyboard Hiding");
    //    NSDictionary* info = [aNotification userInfo];
    //CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    void (^animations)(void) = nil;
    animations = ^{
        // Resize the scroll view to make room for the keyboard
        CGRect aFrame = self.view.frame;
        aFrame.origin.y += distance;//kbSize.height;
        [self.view setFrame: aFrame];
    };
    [UIView animateWithDuration:0.25 animations:animations];
    
    
}

@end
