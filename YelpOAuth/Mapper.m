//
//  Mapper.m
//  YelpOAuth
//
//  Created by Neil Bhargava on 7/24/14.
//  Copyright (c) 2014 Neil Bhargava. All rights reserved.
//

#import "Mapper.h"
#import "DCKeyValueObjectMapping.h"
#import "Business.h"

@implementation Mapper

+(NSArray *) mapBusinessesFromData:(NSData *)data{
    
    NSError *error;
    //NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    //The only useful information is stored in businesses.
    NSArray *json = [[NSJSONSerialization JSONObjectWithData:data options:0 error:&error] objectForKey:@"businesses"];
    NSLog(@"%@", [json[0] objectForKey:@"name"]);
    
    DCKeyValueObjectMapping *parser = [DCKeyValueObjectMapping mapperForClass: [Business class]];
    
    NSMutableArray *businesses = [[NSMutableArray alloc] init];
    for (NSDictionary *kvp in json) {
        Business *b = [parser parseDictionary:kvp];
        NSLog(@"%@ - %@", b.name, b.rating);
        [businesses addObject:b];
    }
    return businesses;
}

@end
