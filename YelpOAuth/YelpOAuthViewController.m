//  YelpOAuthViewController.m
//  YelpOAuth
//
//  Created by Neil Bhargava on 7/23/14.
//  Copyright (c) 2014 Neil Bhargava. All rights reserved.
//

#import "YelpOAuthViewController.h"
#import "Business.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SearchResultTableViewCell.h"

@interface YelpOAuthViewController ()

@property (nonatomic, strong) SearchManager *searchManager;
@property (nonatomic, strong) NSArray *businessArray;

@end

@implementation YelpOAuthViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.searchManager = [[SearchManager alloc] init];
    self.searchManager.delegate = self;
    [self.searchManager searchWithSearch:_search];

	// Do any additional setup after loading the view, typically from a nib.
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SearchResultTableViewCell  *srtvc =  (SearchResultTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if(!srtvc){
        srtvc = [[[NSBundle mainBundle] loadNibNamed:@"SearchResultTableViewCell" owner:self options:nil] objectAtIndex:0];
    }

    Business *b = _businessArray[[indexPath row]];
    srtvc.titleLbl.text = b.name;
    [srtvc.mainImageView sd_setImageWithURL:[NSURL URLWithString:b.image_url] placeholderImage:[UIImage imageNamed:@"60x60.gif"]];
    [srtvc.ratingImageView sd_setImageWithURL:[NSURL URLWithString:b.rating_img_url] placeholderImage:[UIImage imageNamed:@"84x17.gif"]];
    srtvc.reviewCountLbl.text = [NSString stringWithFormat:@"%i Reviews", b.review_count];
    
    return srtvc;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_businessArray count];
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

-(void) didSearchSucceed:(NSArray *)resultArray{
    _businessArray = resultArray;
    dispatch_async(dispatch_get_main_queue(), ^{
        [_businessTableView reloadData];
    });
}

-(void) didSearchFail:(NSString *)error{
        //Insert error handling later...
}


@end
