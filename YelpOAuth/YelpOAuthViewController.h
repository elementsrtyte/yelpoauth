//
//  YelpOAuthViewController.h
//  YelpOAuth
//
//  Created by Neil Bhargava on 7/23/14.
//  Copyright (c) 2014 Neil Bhargava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchManager.h"
#import "Search.h"
#import "BaseViewController.h"

@interface YelpOAuthViewController : BaseViewController <SearchManagerDelegate, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UISearchDisplayDelegate>
@property (weak, nonatomic) IBOutlet UITableView *businessTableView;
@property (strong, nonatomic) Search *search;
@end
