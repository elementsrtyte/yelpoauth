//
//  UserCommunicator.h
//  YelpOAuth
//
//  Created by Neil Bhargava on 7/23/14.
//  Copyright (c) 2014 Neil Bhargava. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Search.h"

@protocol SearchCommunicatorDelegate <NSObject>
    -(void)didSearchSucceed:(NSData *)data;
    -(void)didSearchFail:(NSError *)error;
@end

@interface SearchCommunicator : NSObject

@property (nonatomic, weak) id<SearchCommunicatorDelegate>delegate;
-(void)searchWithSearch:(Search *)s;

@end
