//
//  Crypto.m
//  YelpOAuth
//
//  Created by Neil Bhargava on 7/23/14.
//  Copyright (c) 2014 Neil Bhargava. All rights reserved.
//
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonHMAC.h>
#import "Crypto.h"

@implementation Crypto


+ (NSString *)SHA1withKey:(NSString *)k andData:(NSString *)d {
    
    const char *cKey  = [k cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [d cStringUsingEncoding:NSASCIIStringEncoding];
    
    unsigned char cHMAC[CC_SHA1_DIGEST_LENGTH];
    
    CCHmac(kCCHmacAlgSHA1, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    
    NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
    
    return [HMAC base64EncodedStringWithOptions:0];
}

+ (NSString *)generateNonce{
    //Create algorithm for random string here...
    return @"replace_nonce_with_random_string";
}

@end
