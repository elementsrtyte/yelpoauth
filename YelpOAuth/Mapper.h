//
//  Mapper.h
//  YelpOAuth
//
//  Created by Neil Bhargava on 7/24/14.
//  Copyright (c) 2014 Neil Bhargava. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Mapper : NSObject

+(NSArray *) mapBusinessesFromData:(NSData *)data;

@end
