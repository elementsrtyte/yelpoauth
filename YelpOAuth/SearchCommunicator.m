//
//  UserCommunicator.m
//  YelpOAuth
//
//  Created by Neil Bhargava on 7/23/14.
//  Copyright (c) 2014 Neil Bhargava. All rights reserved.
//

#import "SearchCommunicator.h"
#import "Constants.h"
#import "Crypto.h"
#import "TDOAuth.h"

@implementation SearchCommunicator

-(void) searchWithSearch:(Search *)s{
    NSString *baseURL = @"api.yelp.com";
    NSString *relativeURL = @"/v2/search";    
    NSDictionary *GETParams = [NSDictionary dictionaryWithObjectsAndKeys:s.term, @"term", s.location, @"location", nil];

    NSURLRequest *req = [TDOAuth URLRequestForPath:relativeURL GETParameters:GETParams host:baseURL consumerKey:CONSUMER_KEY consumerSecret:CONSUMER_SECRET accessToken:TOKEN tokenSecret:TOKEN_SECRET];
    
    NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:req completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        [self.delegate didSearchSucceed:data];
    }];
    
    [dataTask resume];

}

@end
