//
//  Business.h
//  YelpOAuth
//
//  Created by Neil Bhargava on 7/25/14.
//  Copyright (c) 2014 Neil Bhargava. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Location;

@interface Business : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *rating;
@property (nonatomic, strong) NSString *rating_img_url;
@property (nonatomic, strong) NSString *image_url;
@property (nonatomic, strong) UIImage *ratingImage;
@property (nonatomic, assign) int review_count;

@end
