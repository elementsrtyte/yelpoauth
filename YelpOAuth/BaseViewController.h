//
//  BaseViewController.h
//  YelpOAuth
//
//  Created by Neil Bhargava on 7/26/14.
//  Copyright (c) 2014 Neil Bhargava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

- (void)keyboardWillBeShown:(NSNotification*)aNotification  moveView:(CGFloat)distance;
- (void)keyboardWillBeHidden:(NSNotification*)aNotification  moveView:(CGFloat)distance;

@end
