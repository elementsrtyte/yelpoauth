//
//  SearchResultTableViewCell.h
//  YelpOAuth
//
//  Created by Neil Bhargava on 7/25/14.
//  Copyright (c) 2014 Neil Bhargava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *mainImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIImageView *ratingImageView;
@property (weak, nonatomic) IBOutlet UILabel *reviewCountLbl;

@end
