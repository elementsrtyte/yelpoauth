//
//  Crypto.h
//  YelpOAuth
//
//  Created by Neil Bhargava on 7/23/14.
//  Copyright (c) 2014 Neil Bhargava. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Crypto : NSObject

+ (NSString *)SHA1withKey:(NSString *)k andData:(NSString *)d;
+ (NSString *)generateNonce;

@end
