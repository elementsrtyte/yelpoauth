//
//  UserManager.m
//  YelpOAuth
//
//  Created by Neil Bhargava on 7/23/14.
//  Copyright (c) 2014 Neil Bhargava. All rights reserved.
//

#import "SearchManager.h"
#import "Business.h"
#import "Mapper.h"

@implementation SearchManager

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.searchCommunicator = [[SearchCommunicator alloc] init];
        self.searchCommunicator.delegate = self;
    }
    return self;
}

-(void) searchWithSearch:(Search *)s{
    [self.searchCommunicator searchWithSearch:s];
}

-(void) didSearchSucceed:(NSData *)data{
    NSArray *businesses = [Mapper mapBusinessesFromData:data];
    
    //TODO: What i should do is store to core data directly from the service.. and then from there load into the tableview.
    [self.delegate didSearchSucceed:businesses];
}


-(void) didSearchFail:(NSError *)error{
    [self.delegate didSearchFail:nil];
}

@end
