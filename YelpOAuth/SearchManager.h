//
//  UserManager.h
//  YelpOAuth
//
//  Created by Neil Bhargava on 7/23/14.
//  Copyright (c) 2014 Neil Bhargava. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SearchCommunicator.h"
#import "Search.h"

@protocol SearchManagerDelegate <NSObject>
    -(void)didSearchSucceed:(NSArray *)resultArray;
    -(void)didSearchFail:(NSString *)error;
@end

@interface SearchManager : NSObject <SearchCommunicatorDelegate>

@property (nonatomic, weak) id<SearchManagerDelegate> delegate;
@property (nonatomic, strong) SearchCommunicator *searchCommunicator;
@property (nonatomic, strong) UIManagedDocument *document;
-(void) searchWithSearch:(Search *)s;

@end
