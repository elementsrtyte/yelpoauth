# README #

Application that uses OAuth 1.0 to use Yelp's API.  Displays results in a UITableview using custom uitableviewcells.

Check the CoreDataManual branch for a simple implementation on how to persist the last search if the user loses internet connection (Uses CoreData).

### How do I get set up? ###

* Run 'pod install' to get all dependencies for this project.  You should be good to go after that.

### Who do I talk to? ###

* Neil Bhargava