//
//  LoginViewController.m
//  YelpOAuth
//
//  Created by Neil Bhargava on 7/23/14.
//  Copyright (c) 2014 Neil Bhargava. All rights reserved.
//

#import "LoginViewController.h"
#import "SearchManager.h"
#import "YelpOAuthViewController.h"

@interface LoginViewController ()

@property (nonatomic, strong) SearchManager *searchManager;
@property (nonatomic, assign) float pixelsToShift;

@end

@implementation LoginViewController

#pragma mark - View Lifecycle Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _searchField.delegate = self;
    _locationField.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [self registerKeyboardNotifications];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [self unregisterKeyboardNotifications];
    [super viewWillDisappear:animated];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    Search *s = [[Search alloc] init];
    s.location = _locationField.text;
    s.term = _searchField.text;
    YelpOAuthViewController *vc = segue.destinationViewController;
    vc.search = s;
    
    //Ensure keyboards down for smoothness.
    [_searchField resignFirstResponder];
    [_locationField resignFirstResponder];
}


#pragma mark - TextField Observers and Behavior
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void) registerKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(keyboardWillBeShown:moveView:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:moveView:) name:UIKeyboardWillHideNotification object:nil];
}

-(void) unregisterKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}


-(void) keyboardWillBeShown:(NSNotification *)aNotification moveView:(CGFloat)distance{
    [super keyboardWillBeShown:aNotification moveView:100];
}

-(void) keyboardWillBeHidden:(NSNotification *)aNotification moveView:(CGFloat)distance{
    [super keyboardWillBeHidden:aNotification moveView:100];
}

@end
